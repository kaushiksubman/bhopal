FROM walkon/baseimage
# App listens to 3000
EXPOSE 3000

CMD [ "npm", "run", "start-worker" ]
