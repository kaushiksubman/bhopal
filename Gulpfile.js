// noinspection JSUnusedGlobalSymbols
var gulp = require('gulp');
var jscs = require('gulp-jscs');
var jshint = require('gulp-jshint');
var stylish = require('jshint-stylish');
var istanbul = require('gulp-istanbul');
var mocha = require('gulp-mocha');
var del = require('del');
var runSequence = require('run-sequence');
var replace = require('gulp-replace');
var serverStartCommand = 'CMD [ "npm", "start" ]';
var workerStartCommand = 'CMD [ "npm", "run", "start-worker" ]';

gulp.task('clean', function (cb) {
  return del(['dist/', 'logs/*.log*', 'nohup.out', '.tmp', 'coverage', 'out'],
    cb);
});

gulp.task('git-hook', function () {
  gulp.src(['git-hooks/**'])
    .pipe(gulp.dest('.git/hooks'));
});

/*gulp.task('build', function () {
 return gulp.src(['**',
 '!{node_modules,node_modules/!**,README.md,nohup.out,logs,logs/!**}',
 '!{specs.md,Vagrantfile,.idea,.idea/!**,.strong-pm,.strong-pm/!**}',
 '!{.vagrant,.vagrant/!**,.editorconfig,.flowconfig,.elasticbeanstalk}',
 '!{.elasticbeanstalk/!**}'
 ],
 {dot: true})
 .pipe(gulp.dest('dist'));
 });*/

gulp.task('copy-tmp-files', function () {
  return gulp.src(['Dockerfile'])
    .pipe(gulp.dest('.tmp'));
});

gulp.task('setup-worker', function () {
  return gulp.src(['.tmp/Dockerfile'])
    .pipe(replace(serverStartCommand,
      workerStartCommand))
    .pipe(gulp.dest('.'));
});

gulp.task('build-worker', function (callback) {
  return runSequence('copy-tmp-files', 'setup-worker', 'clean',
    callback);
});


gulp.task('setup-server', function () {
  return gulp.src(['.tmp/Dockerfile'])
    .pipe(replace(workerStartCommand,
      serverStartCommand))
    .pipe(gulp.dest('.'));
});

gulp.task('build-server', function (callback) {
  return runSequence('copy-tmp-files', 'setup-server', 'clean',
    callback);
});

function getVersionInfo() {
  return {
    version: 'v1.0.' + (process.env.CI_BUILD_NUMBER || '0'),
    CI: process.env.CI || null,
    CI_BUILD_NUMBER: process.env.CI_BUILD_NUMBER || null,
    CI_BRANCH: process.env.CI_BRANCH || null,
    CI_COMMIT_ID: process.env.CI_COMMIT_ID || null,
    CI_COMMITTER_NAME: process.env.CI_COMMITTER_NAME || null,
    CI_COMMITTER_EMAIL: process.env.CI_COMMITTER_EMAIL || null,
    CI_COMMITTER_USERNAME: process.env.CI_COMMITTER_USERNAME || null,
    CI_MESSAGE: process.env.CI_MESSAGE || null
  };
}
gulp.task('gen-version-info', function (callback) {

  var fs = require('fs');
  fs.writeFileSync('server/version-info.json',
    JSON.stringify(getVersionInfo(), null, 2));
  callback();
});

gulp.task('verify-deployment', function (callback) {
  var boot = require('loopback-boot');
  var request = require('supertest');
  var assert = require('assert');
  var appConfig = boot.ConfigLoader.loadAppConfig(__dirname + '/server',
    process.env.NODE_ENV);
  var baseUrl = appConfig.baseUrl;

  function verifyResult(err, res) {
    assert.equal(err, null);
    assert.ok(res);
    var serverConfigs = res.body;
    assert.equal(appConfig.baseUrl, serverConfigs.baseUrl);
    assert.equal(appConfig.webAppUrl, serverConfigs.webAppUrl);
    assert.equal(appConfig.restApiRoot, serverConfigs.restApiRoot);
    assert.equal(appConfig.imageCDNUrl, serverConfigs.imageCDNUrl);

    assert.equal(appConfig.passportProviders['google-plus'].callbackURL,
      serverConfigs.gplusCallbackUrl);
    assert.equal(appConfig.playStoreUrl, serverConfigs.playStoreUrl);
    assert.equal(true, serverConfigs.amqpConnected);
    assert.equal(process.env.NODE_ENV || 'undefined',
      serverConfigs.env.NODE_ENV);

    assert.deepEqual(getVersionInfo(), serverConfigs.versionInfo);
  }

  request(baseUrl).get('/configs').end(function (err, res) {
    verifyResult(err, res);
    request(baseUrl).get('/worker-configs').end(function (err, res) {
      verifyResult(err, res);
      callback();
    });
  });

});

gulp.task('jscs', function () {
  return gulp.src(['server/**/*.js', 'common/**/*.js'],
    {dot: true})
    .pipe(jscs());
});

gulp.task('lint', function () {
  return gulp.src(['server/**/*.js', 'common/**/*.js', 'test/**/*.js'])
    .pipe(jshint())
    .pipe(jshint.reporter(stylish))
    .pipe(jshint.reporter('fail'));
});

gulp.task('test', function (cb) {
  gulp.src(['server/**/*.js', 'common/**/*.js'])
    .pipe(istanbul()) // Covering files
    .pipe(istanbul.hookRequire()) // Force `require` to return covered files
    .on('finish', function () {
      gulp.src(['test/**/*.js'])
        .pipe(mocha())
        .pipe(istanbul.writeReports()) // Creating the reports after tests ran
        // jscs:disable maximumLineLength
        .pipe(istanbul.enforceThresholds({thresholds: {global: 33}})) // Enforce a coverage of at least 90%
        // jscs:enable maximumLineLength
        .on('end', function () {
          process.exit()
        });
    });
});

gulp.task('default', function (callback) {
  return runSequence('clean', callback);
});