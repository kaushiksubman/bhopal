Walkon API Project
================

##Setting Up Local Server
###Install Nodejs - [https://nodejs.org/download/](https://nodejs.org/download/)
###Install MongoDB - [https://www.mongodb.org/downloads](https://www.mongodb.org/downloads)
After installing MongoDB, you also need to start it.
```
mongod
```
###Install RabbitMQ - [https://www.rabbitmq.com/download.html](https://www.rabbitmq.com/download.html)

###Clone & Install Dependencies
Install the dependencies by running `npm install` in the project directory.
```
git clone git@bitbucket.org:walkonsocial/walkon-api.git
cd walkon-api
npm install
```

###Start Worker
The main server script is located at `server/worker.js`. Start it by running the command.
```
node server/worker.js
```

###Start Server
The main server script is located at `server/server.js`. Start it by running the command.
```
node server/server.js
```

##[Latest BDD Results](/walkonsocial/walkon-api/src/master/specs.md)
Walkon is developed using Behavior Driven Development methodology. The latest BDD results indicate the working features of the API. The latest result is available [here](/walkonsocial/walkon-api/src/master/specs.md).

###Release Notes
https://bitbucket.org/walkonsocial/walkon-api/wiki/Release%20Notes