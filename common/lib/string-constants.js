/* @flow weak */
/**
 * Created by ningsuhen on 4/21/15.
 */

exports = module.exports = {
  DEPENDENCY_LOADED: 'events.dependencyLoaded',
  EVENT_MONGODB_CONNECTED: 'events.mongodbConnected'
};
