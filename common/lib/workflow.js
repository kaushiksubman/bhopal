/**
 * Created by ningsuhen on 6/26/15.
 */
var events = require('events');
var util = require('util');

function Workflow() {

}
util.inherits(Workflow, events.EventEmitter);

Workflow.prototype.reportError = function (err, cb) {
  if (err) {
    return this.emit('error', err);
  }
  if (typeof cb === 'function') {
    cb();
  }
};

exports = module.exports = Workflow;
