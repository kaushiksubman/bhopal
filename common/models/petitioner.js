/* @flow weak */
var uid = require('rand-token').uid;
module.exports = function (Petitioner) {
  Petitioner.validatesUniquenessOf('email');

  Petitioner.findAndModify =
    function findAndModify(query, sort, updateData, options, cb) {
      if (!query._id && query.id) {
        query._id = query.id;
        delete query.id;
      }
      if (!cb && typeof options === 'function') {
        cb = options;
        options = null;
      }
      Petitioner.app.dataSources.mongodb.connector.collection('Petitioner').
        findAndModify(query, sort, updateData, options, cb);
    };

  Petitioner.observe('before save', function (ctx, next) {
  	if (!ctx.instance || !ctx.isNewInstance) {
  		return next();
  	}
    if (ctx.instance.firstName && ctx.instance.lastName) {
      ctx.instance.firstName = ctx.instance.firstName.replace(/[^A-Z0-9]+/ig, '');
      ctx.instance.lastName = ctx.instance.lastName.replace(/[^A-Z0-9]+/ig, '');
      ctx.instance.email = ctx.instance.firstName.toLowerCase() + '.' +
        ctx.instance.lastName.toLowerCase() + '@justiceforbhopal.in';
      Petitioner.findOne({
        where: {
          email: ctx.instance.email
        }
      }, function (err, petitioner) {
        if (err) {
          return next(err);
        }
        if (!petitioner) {
          return next();
        }
        var random = uid(4).toLowerCase();
        ctx.instance.email = ctx.instance.firstName.toLowerCase() + '.' +
          ctx.instance.lastName.toLowerCase() + random + '@justiceforbhopal.in';
        next();
      });
    	return;
    }
    next();
  });
};