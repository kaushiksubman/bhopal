/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
var environments = {
  staging: {
    key: '3fba56b2d1e64cd018fbd87c4a6bd7a8805c428d',
    appName: 'Just Move Stage'
  },
  production: {
    key: 'c2f0e5e170f611d34b0a0d42068ab650441b17b4',
    appName: 'Just Move API'
  }
};
var envName = process.env.NODE_ENV || 'development';
var config = environments[envName] || {
    key: '3fba56b2d1e64cd018fbd87c4a6bd7a8805c428d',
    appName: 'Just Move Default'
  };
exports.config = {
  /**
   * Array of application names.
   */
  app_name: [config.appName], // jshint ignore:line
  /**
   * Your New Relic license key.
   */
  license_key: config.key, // jshint ignore:line
  logging: {
    /**
     * Level at which to log. 'trace' is most useful to New Relic when
     * diagnosing issues with the agent, 'info' and higher will impose the least
     * overhead on production applications.
     */
    level: 'info'
  }
};
