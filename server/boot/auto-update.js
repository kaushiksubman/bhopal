/**
 * Created by ningsuhen on 5/2/15.
 */
module.exports = function autoUpdateIndexes(app) {
  var logger = require('../logger');
  var log = logger.child({
    module: 'AutoUpdate'
  });
  app.dataSources.mongodb.autoupdate(function (err) {
    if (err) {
      return log.error(err, 'error updating schema');
    }
  });
};
