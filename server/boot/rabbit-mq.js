/**
 * Created by ningsuhen on 4/21/15.
 */
var jackrabbit = require('jackrabbit');
var rpc = require('rabbit-rpc');
var SConstants = require('../../common/lib/string-constants');
module.exports = function (app) {
  var amqpUrl = app.get('rabbitMQ').url;
  var queue = app.rabbitQueue = app.rabbitQueue || jackrabbit(amqpUrl);
  app.rabbitRPC = app.rabbitRPC || rpc(amqpUrl);
  queue.available = {};
  queue.on('connected', function () {
    queue.isConnected = true;
  });
};
