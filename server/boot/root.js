module.exports = function (server) {


  var utils = require('../lib/utils')(server);
  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  router.get('/', server.loopback.status());
  router.get('/configs', function (req, res) {
    var data = utils.getConfigs();
    res.json(data);
  });
  router.get('/worker-configs', function (req, res) {
    if (!server.rabbitQueue.isConnected) {
      return res.json({error: 'rabbitMQ not connected'});
    }
    var rpc = server.rabbitRPC;
    rpc.call('utils.getConfigs', function (err, result) {
      res.json(result);
    });
  });

  router.get('/robots.txt', function (req, res) {
    res.header('Content-Type', 'text/plain');
    res.send('User-agent: * \nDisallow: /');
  });
  server.use(router);
};
