/**
 * Created by ningsuhen on 4/30/15.
 */
/*jslint maxlen: 160 */
// jscs: disable maximumLineLength
// noinspection SpellCheckingInspection
var amqpUri = process.env.CLOUDAMQP_URI ||
  process.env.AMQP_URI ||
  'amqp://mokjpyjc:jzVgES5BxbFgYXmojgZNJyZB73h5WfgI@jellyfish.rmq.cloudamqp.com/mokjpyjc';

var baseUrl = process.env.BASE_URL ||
  'http://api.justiceforbhopal.in';

var webAppUrl = process.env.WEB_APP_URL ||
  'http://web.justiceforbhopal.in';

var appName = process.env.APP_NAME || 'Bhopal';

module.exports = {
  appName: appName,
  baseUrl: baseUrl,
  webAppUrl: webAppUrl,
  restApiRoot: '/v1',
  disableExplorer: true,
  rabbitMQ: {
    url: amqpUri
  },
  logEntries: {
    apiKey: '2e5f7fcf-03e5-4f5a-84d3-34afbde704c5',
    level: 'info'
  },
  errorHandler: {
    disableStackTrace: true
  }
};
