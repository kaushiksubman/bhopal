/**
 * Created by ningsuhen on 4/2/15.
 */
/*jslint maxlen: 160 */
// jscs: disable maximumLineLength
var mongoUri = process.env.MONGOLAB_URI || process.env.MONGOHQ_URL ||
  'mongodb://bhopal:bhopal123@ds011893.mlab.com:11893/bhopal';
module.exports = {
  mongodb: {
    url: mongoUri
  }
};
