/**
 * Created by ningsuhen on 4/22/15.
 */
var SConstants = require('../../common/lib/string-constants');
var async = require('async');
var schedule = require('node-schedule');
var _ = require('lodash');
var moment = require('moment-timezone');
var os = require('os');
var debug = require('debug')('walkon:jobs');
var request = require('request-promise');
var Mailgun = require('mailgun').Mailgun;
var mg = new Mailgun('key-99595738ce36909bc17a58f94284d2e3');
var uid = require('rand-token').uid;

/**
 *
 * Logging
 */
var logger = require('../logger');
var log = logger.child({module: 'jobs'});

/**
 *
 * Initialize Variables
 */
var app = null;
var queue = null;
var rpc = null;

/**
 * Scan Petitioners to sign petition
 * @callback cb
 */
function scanPetitioners(cb) {
  // scan OtpKeys expiring in the next hour
  var petitionId = '2265901';
  var apiKey = 'O7fYaZdmTh92J6YYcdursx0QYIzYSB2QcDrW0O9Q';
  app.models.Petitioner.findAndModify({
    status: 'incomplete'
  }, null, {
    $set: {
      status: 'complete'
    }
  }, function (err, result) {
    if (err) {
      return cb(err);
    }
    if (result && result.ok && result.value) {
      var petitionerData = result.value;
      petitionerData.id = petitionerData._id;
      var petitioner = new app.models.Petitioner(petitionerData,
        {fields: undefined, applySetters: false, persisted: true});
      var email = petitioner.email;
      var firstName = petitioner.firstName;
      var lastName = petitioner.lastName;
      var options = {  
        method: 'POST',
        uri: 'https://api.whitehouse.gov/v1/signatures.json',
        body: {
          petition_id: petitionId,
          email: email,        
          first_name: firstName,
          last_name: lastName
        },
        qs: {
          api_key: apiKey
        },
        json: true
      };

      request(options)  
        .then(function (response) {
          log.info({response: response},
            'Petition signed succesfully for ' + email);
          cb();
        })
        .catch(function (err) {
          // Deal with the error
          log.error({error: err},
            'Error signing petition for ' + email);
          petitioner.updateAttributes({
            status: 'errored'
          }, function (err, petitioner) {
            if (err) {
              return cb(err);
            }
            cb();
          });
        });
      return;
    }
    log.info('No more petitioners left');
    cb();
  });
}

/**
 * Scan Petitioners for duplicates
 * @callback cb
 */
function scanDuplicates(cb) {
  app.dataSources.mongodb.connector.collection('Petitioner').
    aggregate([{
      $group: {
        _id: '$email',
        count: {
          $sum: 1 
        } 
      } 
    },
    {
      $match: {
        _id : {
          $ne : null 
        },
        count : {
          $gt: 1
        }
      }
    },
    {
      $project: {
        email : '$_id',
        _id : 0
      }
    }, {
      $limit: 1
    }], function (err, results) {
      if (err) {
        return cb(err);
      }
      if (results.length < 1) {
        return cb();
      }
      async.each(results, function (duplicate, next) {
        app.models.Petitioner.find({
          where: {
            email: duplicate.email
          },
          skip: 1
        }, function (err, petitioners) {
          if (err) {
            return next(err);
          }
          async.each(petitioners, function (petitioner, done) {
            var random = uid(4).toLowerCase();
            petitioner.firstName = petitioner.firstName.
              replace(/[^A-Z0-9]+/ig, '');
            petitioner.lastName = petitioner.lastName.
              replace(/[^A-Z0-9]+/ig, '');
            var email = petitioner.firstName.toLowerCase() + '.' +
              petitioner.lastName.toLowerCase() + random +
              '@justiceforbhopal.in';
            app.models.Petitioner.
              findAndModify({
                _id: petitioner.id
              }, null, {
                $set: {
                  email: email,
                  status: 'duplicate'
                }
              }, {new: true}, function (err, result) {
                if (err) {
                  return done(err);
                }
                if (result && result.ok && result.value) {
                  done();
                  return;
                }
                done();
              });
          }, function (err) {
            if (err) {
              return next(err);
            }
            next();
          });
        });
      }, function (err) {
        if (err) {
          return cb(err);
        }
        process.nextTick(function () {
          scanDuplicates(cb);
        });
      });
    });
}

function scanAndScheduleJobs(cb) {
  log.info('running scanAndScheduleJobs');

  var completed = false;
  setTimeout(function () {
    if (!completed) {
      log.error('scanAndScheduleJobs not completed in 30 minutes');
    }
  }, 1000 * 60 * 30);
  async.parallel([
    exports.scanPetitioners
  ], function (err, info) {

    if (err) {
      log.error({error: err, info: info}, 'error running scanAndScheduleJobs');
    }
    // Run the job every ten minutes but at an arbitrary minute - to distribute the job
    //var randomMinute = Math.floor(Math.random() * 5);
    // schedule hourly jobs
    var now = new Date();
    /*if (randomMinute < 3) {
      randomMinute = 3;
    }*/
    var randomMinute = 2.5;
    var scheduleDate = new Date(now.getTime() + randomMinute * 60000);
    schedule.scheduleJob(scheduleDate, scanAndScheduleJobs);
    log.info('scanAndScheduleJobs parallel tasks completed');
    completed = true;
    process.nextTick(function () {
      if (cb) {
        cb();
      }
    });
  });
}

exports = module.exports = {
  configure: function (appInstance) {
    app = appInstance;
    queue = app.rabbitQueue;
    rpc = app.rabbitRPC;
  },
  scanAndScheduleJobs: scanAndScheduleJobs,
  scanPetitioners: scanPetitioners,
  scanDuplicates: scanDuplicates
};
