/**
 * Created by ningsuhen on 5/1/15.
 */

function init(app) {
  var rpc = app.rabbitRPC;


  var logger = require('../logger');
  var log = logger.child({module: 'rpc'});
  var utils = require('./utils')(app);

  function toPlainObject(obj) {
    if (!obj) {
      return obj;
    }
    var plainObj = obj;
    try {
      plainObj = JSON.parse(JSON.stringify(obj));
    }
    catch (err) {
      log.error({error: err, object: obj}, 'Error flattening object');
    }
    return plainObj;

  }

  /*rpc.handle('user.syncFitness',
    function (userId, googleFitData, meta, callback) {
      app.models.user.findById(userId, function (err, currUser) {
        if (err) {
          log.error(err, 'Error finding user with id %s', userId);
          callback(toPlainObject(err));
        }
        if (!currUser) {
          var e = new Error('user not found for id ' + userId +
            ' rpc user.syncFitness');
          log.error(e);
          return callback(toPlainObject(e));
        }
        currUser.syncFitness(googleFitData, meta, function (err, result) {
          callback(toPlainObject(err), toPlainObject(result));
        });
      });
    });*/
}


exports = module.exports = init;
