var url = require('url');
module.exports = function (app) {

  var versionInfo = {};
  try {
    versionInfo = require('../version-info.json');
  }
  catch (e) {
  }
  function secureUri(uri) {
    var uriParsed = url.parse(uri);
    // hide credentials
    uriParsed.auth = null;
    uriParsed.href = null;
    return uriParsed.hostname + (uriParsed.path || '');
  }

  function getConfigs() {
    var amqpUri = app.get('rabbitMQ').url;
    var mongodbUri = app.dataSources.mongodb.settings.url;
    return {
      appName: app.get('appName'),
      baseUrl: app.get('baseUrl'),
      webAppUrl: app.get('webAppUrl'),
      restApiRoot: app.get('restApiRoot'),
      disableExplorer: app.get('disableExplorer'),
      amqpConnected: app.rabbitQueue.isConnected,
      env: {
        NODE_ENV: process.env.NODE_ENV || 'undefined',
        LOG_PATH: process.env.LOG_PATH || 'undefined',
        WORKER: process.env.WORKER || 'undefined',
        THRIFTY: process.env.THRIFTY || 'undefined',
        FORCE_LISTEN: process.env.FORCE_LISTEN || 'undefined'
      },
      versionInfo: versionInfo,
      amqpUri: secureUri(amqpUri),
      mongodbUri: secureUri(mongodbUri),
      s3Bucket: bucketName
    };
  }

  return {
    getConfigs: getConfigs
  };
};

