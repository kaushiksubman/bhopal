/**
 * Created by ningsuhen on 4/26/15.
 */
var boot = require('loopback-boot');
/**
 *
 * @returns {{child:Function}}
 * @constructor {{child:Function}}
 */
function Logger() {
  var logEntriesLevels = {
    trace: 'debug',
    debug: 'debug',
    info: 'info',
    warn: 'warning',
    error: 'err',
    fatal: 'emerg'
  };

  var path = require('path');
  var appConfig = boot.ConfigLoader.loadAppConfig(__dirname,
    process.env.NODE_ENV);
  var bunyan = require('bunyan');

  var logEntriesApiKey = appConfig.logEntries.apiKey;
  var logEntriesLevel = appConfig.logEntries.level || 'info';
  var logEntriesStream = require('logentries-stream')(logEntriesApiKey,
    logEntriesLevels[logEntriesLevel]);

  var logLevels = appConfig.logging.levels;

  var defaultLogLevel = logLevels.default || 'info';
  var stdOutLogLevel = logLevels.stdout;
  /**
   * @type {{child:Function}}
   */
  var logFilePath = process.env.LOG_PATH || path.join(__dirname, '../logs');

  var applicationType = 'server';
  if (process.env.WORKER && process.env.WORKER === 'true') {
    applicationType = 'worker';
  }
  var applicationName = 'walkon-' + applicationType;
  var logFile = path.join(logFilePath, applicationName + '.log');

  var streams = [
    {level: logEntriesLevel, stream: logEntriesStream},
    {
      type: 'rotating-file',
      path: logFile,
      period: '1d',   // daily rotation
      count: 3        // keep 3 back copies
    }
  ];
  if (stdOutLogLevel) {
    streams.push({stream: process.stdout, level: stdOutLogLevel});
  }
  return bunyan.createLogger({
    name: applicationName,
    streams: streams,
    level: defaultLogLevel
  });
}
/**
 *
 * @type {{child:Function}}
 */
exports = module.exports = new Logger();
