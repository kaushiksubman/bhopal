/**
 * Created by ningsuhen on 4/2/15.
 */

var app = require('../server');
var logger = require('../logger');
module.exports = function (options) {
  var log = logger.child({module: 'default-scope-middleware'});
  return function addDefaultScope(req, res, next) {
    var filter = req.query.filter || {};
    if (typeof filter === 'string') {
      try {
        filter = JSON.parse(filter);
      }
      catch (e) {
        log.error(e, 'Error parsing filter parameter');
      }
    }
    filter.limit = filter.limit || 10;
    filter.order = filter.order || 'created DESC';
    req.query.filter = filter;
    if (req.body && req.body.created) {
      delete req.body.created;
    }
    next();
  };
};
