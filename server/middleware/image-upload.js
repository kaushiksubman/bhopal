/**
 * Created by ningsuhen on 4/2/15.
 */

var app = require('../server');
var dataSources = require('../datasources.json');
var uid = require('rand-token').uid;
var AuthError = require('../../common/lib/custom-errors').AuthError;
var RequestError = require('../../common/lib/custom-errors').RequestError;
var ServerError = require('../../common/lib/custom-errors').ServerError;
var logger = require('../logger');
module.exports = function (options) {
  var log = logger.child({module: 'image-upload'});
  return function imageUploadApi(req, res, next) {
    var AccessToken = app.models.AccessToken;
    AccessToken.findForRequest(req, function (err, accessToken) {
      if (err) {
        log.error(err, 'Error finding access token in image upload api');
        return next(err);
      }
      if (accessToken === undefined) {
        var e = new AuthError(AuthError.AUTHENTICATION_REQUIRED);
        res.status(401);
        res.send({
          error: e
        });
      } else {
        // noinspection JSUnusedGlobalSymbols
        var uploadOptions = {
          container: dataSources.storage.bucketName,
          acl: 'public-read',
          allowedContentTypes: ['image/png', 'image/jpeg'],
          maxFileSize: 5 * 1024 * 1024,
          getFilename: function (fileInfo) {
            var date = new Date();
            // accessToken.userId + '/' +  // should not append userId in front?
            return date.getUTCFullYear() + '/' + date.getUTCMonth() + '/' +
              date.getUTCDate() + '/' + (uid(12) + '-' +
              encodeURIComponent(fileInfo.name)).toLowerCase();
          }
        };
        app.models.container.upload(req,
          res, uploadOptions,
          function (err, result) {
            if (err) {
              return next(err);
            }
            if (!result.files.hasOwnProperty('file')) {
              var e = new RequestError(RequestError.NO_FILE_UPLOADED);
              e.details = 'No file uploaded. `file` parameter can\'t be blank';
              return res.json(e);
            }
            app.models.user.findById(accessToken.userId, function (err, user) {
              if (err) {
                return next(err);
              }
              if (!user) {
                var e = new RequestError(RequestError.USER_NOT_FOUND);
                e.details = 'User not Found. User account is probably deleted';
                res.status(404);
                return res.json({error: e});
              }
              var fileInfo = result.files.file[0];
              user.images.create({
                  filename: fileInfo.name,
                  container: fileInfo.container,
                  url: 'http://' + fileInfo.container +
                  '.s3.amazonaws.com/' + fileInfo.name
                },
                function (err, image) {
                  if (err) {
                    return next(err);
                  }
                  res.json(image);
                  res.send();
                });
            });
          });
      }
    });
  };
};
