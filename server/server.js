var loopback = require('loopback');
var boot = require('loopback-boot');
var app = module.exports = loopback();
var debug = require('debug')('walkon:server');
var logger = require('./logger');
var log = logger.child({module: 'server'});
var bodyParser = require('body-parser');
var Multer = require('multer');
var upload = Multer({ dest: 'uploads/' })
var Parse = require('csv-parse');
var fs = require('fs');
var async = require('async');
var validator = require('json-gate').createSchema;
var request = require('request-promise');
/*// Enable http session if server side oAuth is required
 app.use(loopback.session({ secret: 'keyboard cat' }));*/

// -- Add your pre-processing middleware here --
app.use(loopback.context());
app.use(loopback.token());
app.use(bodyParser.json({limit: '5mb'}));
// Middleware to log response time of all APIs.

app.start = function () {
  // start the web server
  return app.listen(function () {
    app.emit('started');
    console.log('Web server listening at: %s', app.get('url'));
  });
};

/**
 *
 */
// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
  if (err) {
    throw err;
  }

  var expressBunyan = require('express-bunyan-logger');
  var logEntriesApiKey = app.get('logEntries').apiKey;
  var logEntriesLevel = app.get('logEntries').level || 'info';
  var logEntriesStream = require('logentries-stream')(logEntriesApiKey,
    logEntriesLevel);
  var logLevels = app.get('logging').levels;

  var defaultLogLevel = logLevels.default || 'info';

  app.use(expressBunyan({
    name: 'walkon-app-express',
    streams: [{level: logEntriesLevel, stream: logEntriesStream},
      {stream: process.stdout, level: 'error'}],
    level: defaultLogLevel
  }));

  function parseCSVFile (sourceFilePath, columns, onNewRecord, handleError, done) {
    var source = fs.createReadStream(sourceFilePath);
    var linesRead = 0;
    var parser = Parse({
      delimiter: ',', 
      columns:columns
    });

    parser.on('readable', function () {
      var record;
      while (record = parser.read()) {
        linesRead++;
        onNewRecord(record);
      }
    });

    parser.on('error', function(error){
      handleError(error)
    });

    parser.on('end', function(){
      done(linesRead);
    });

    source.pipe(parser);
  }

  //We will call this once Multer's middleware processed the request
  //and stored file in req.files.fileFormFieldName

  function parseVolunteerFile (req, res) {
      var filePath = req.file.path;
      var volunteers = [];
      function onNewRecord(record){
        volunteers.push(record);
      }

      function onError(error){
        res.status(500).send(error);
      }

      function done(linesRead){
        async.each(volunteers, function (volunteer, next) {
          app.models.Volunteer.findOne({
            where: {
              email: volunteer.email
            }
          }, function (err, dbVolunteer) {
            if (err) {
              return next(err);
            }
            if (dbVolunteer) {
              return next();
            }
            app.models.Volunteer.create({
              email: volunteer.email
            }, next)
          });
        }, function (err) {
          if (err) {
            res.status(500).send(error);
          }
          res.status(200).send({volunteersAdded: linesRead});
        });
      }

      var columns = true; 
      parseCSVFile(filePath, columns, onNewRecord, onError, done);

  }

  function parsePetitionerFile (req, res) {
      var filePath = req.file.path;
      var petitioners = [];
      function onNewRecord(record){
        petitioners.push(record);
      }

      function onError(error){
        res.status(500).send(error);
      }

      function done(linesRead){
        async.each(petitioners, function (petitioner, next) {
          var firstName = petitioner.firstName;
          var lastName = petitioner.lastName;
          if(!firstName || !lastName) {
            return next();
          }
          firstName = firstName.replace(/[^A-Z0-9]+/ig, '');
          lastName = lastName.replace(/[^A-Z0-9]+/ig, '');
          var email;
          email = firstName.toLowerCase() + '.' +
            lastName.toLowerCase() + '@justiceforbhopal.in';
          app.models.Petitioner.create({
            firstName: firstName,
            lastName: lastName,
            email: email
          }, function (err, petitioner) {
            if (err) {
              log.error({error: err, firstName: firstName, lastName: lastName},
                'Error creating petitioner');
            }
            next();
          });
        }, function (err) {
          if (err) {
            res.status(500).send(error);
          }
          res.status(200).send({petitionersAdded: linesRead});
        });
      }

      var columns = true; 
      parseCSVFile(filePath, columns, onNewRecord, onError, done);

  }

  //this is the route handler with two middlewares. 
  //First:  Multer middleware to download file. At some point,
  //this middleware calls next() so process continues on to next middleware
  //Second: use the file as you need

  app.post('/uploadVol', upload.single('file'), parseVolunteerFile);
  app.post('/uploadPet', upload.single('file'), parsePetitionerFile);

  function validatePetition (petition, cb) {
    var objectSchema = {
      type: 'object',
      disallow: ['string', 'number', 'integer', 'boolean',
        'array', 'null'],
      properties: {
        firstName: {
          type: 'string',
          disallow: ['object', 'number', 'integer', 'boolean',
            'array', 'null'],
          minLength: 1,
          required: true
        },
        lastName: {
          type: 'string',
          disallow: ['object', 'number', 'integer', 'boolean',
            'array', 'null'],
          minLength: 1,
          required: true
        },
        email: {
          type: 'string',
          disallow: ['object', 'number', 'integer', 'boolean',
            'array', 'null'],
          minLength: 1,
          format: 'email',
          required: true
        }
      },
      additionalProperties: false,
      required: true
    };
    var schema = validator(objectSchema);

    schema.validate(petition, cb);
  }
  app.post('/jhatkaaPetition', function (req, res) {
    var petition = req.body;
    validatePetition(petition, function (err) {
      if (err) {
        var e = {};
        e.status = 'error';
        e.type = 'invalidInput';
        return res.json(e);
      }
      var firstName = petition.firstName;
      var lastName = petition.lastName;
      firstName = firstName.replace(/[^A-Z0-9]+/ig, '');
      lastName = lastName.replace(/[^A-Z0-9]+/ig, '');
      var email = petition.email;
      var status = 'complete';
      var petitionId = '2265901';
      var apiKey = 'O7fYaZdmTh92J6YYcdursx0QYIzYSB2QcDrW0O9Q';
      var options = {  
        method: 'POST',
        uri: 'https://api.whitehouse.gov/v1/signatures.json',
        body: {
          petition_id: petitionId,
          email: email,        
          first_name: firstName,
          last_name: lastName
        },
        qs: {
          api_key: apiKey
        },
        json: true
      };
      app.models.Petitioner.create({
        firstName: firstName,
        lastName: lastName,
        email: email,
        status: status,
        source: 'Jhatkaa'
      }, function (err, signedPetition) {
        if (err) {
          log.error({error: err, petition: petition},
            'Error signing petition - Jhatkaa');
          // Handle error gracefully
        }
        request(options)  
          .then(function (response) {
            log.info({response: response},
              'Petition signed succesfully for ' + email);
            return res.json(response);
          })
          .catch(function (err) {
            // Deal with the error
            log.error({error: err},
              'Error signing petition for ' + email);
            return res.json(err);
          });        
      });
    });
  });
  // Start the server if `$ node server.js`
  if (require.main === module) {
    app.start();
    process.on('uncaughtException', function (err) {
      // Log the error
      log.fatal(err, 'Uncaught Exception in server.js');
      process.exit();
    });
  } else if (process.env.THRIFTY === 'true' ||
    process.env.FORCE_LISTEN === 'true') {
    app.start();
  }
  app.emit('loaded');
});
process.on('SIGINT', function () {
  process.exit();
});
