/**
 * Created by ningsuhen on 4/21/15.
 */
process.env.WORKER = 'true';
var app = require('./server');

var SConstants = require('../common/lib/string-constants');
var async = require('async');
var events = require('events');
var _ = require('lodash');
var jobs = require('./lib/jobs');
var schedule = require('node-schedule');

var logger = require('./logger');
var log = logger.child({module: 'worker'});


function WorkerFactory(app) {

  // this is needed to configure the jobs
  jobs.configure(app);

  function scheduleJobs() {
    jobs.scanAndScheduleJobs();
  }

  function startRPCHandlers() {
    var rpc = require('./lib/rpc')(app);
  }

  var loaded = false;
  return {
    app: app,
    init: function (onInitComplete) {
      if (loaded) {
        return onInitComplete();
      }
      setTimeout(function () {
        if (!loaded) {
          var err =
            new Error('Worker Dependencies not loaded after 20 seconds.');
          log.error(err);
          throw err;
        }
      }, 20000);
      function onLoad() {
        // dependent queues are up.
        scheduleJobs();
        startRPCHandlers();
        loaded = true;
        if (onInitComplete) {
          onInitComplete();
        }
      }

      var queue = app.rabbitQueue;
      var rpc = app.rabbitRPC;
      var dependencyManager = (function () {
        var obj = new events.EventEmitter();
        obj.items = [];
        obj.push = function (dependencyName) {
          obj.items.push(dependencyName);
          obj.emit(SConstants.DEPENDENCY_LOADED);
        };
        return obj;
      })();

      // Mongodb connection Dependency
      var mongodbConnection = app.dataSources.mongodb.connector.db;
      if (!mongodbConnection) {
        app.dataSources.mongodb.once('connected', function () {
          log.info('mdb once connected');
          dependencyManager.push(SConstants.EVENT_MONGODB_CONNECTED);
        });
      } else {
        log.info('mdb already connected');
        dependencyManager.push(SConstants.EVENT_MONGODB_CONNECTED);
      }

      dependencyManager.on(SConstants.DEPENDENCY_LOADED, function () {
        var dependencies = [SConstants.EVENT_MONGODB_CONNECTED];
        var loadedDependencies = _.intersection(dependencyManager.items,
          dependencies);
        log.info({loadedDependencies: dependencyManager.items});
        if (loadedDependencies.length === dependencies.length) {
          onLoad();
        }
      });
      function waitForQueueAvailability(queueName, cb) {
        if (queue.available[queueName]) {
          log.info(queueName + ' already available');
          dependencyManager.push(queueName);
          cb();
        } else {
          queue.once('events.' + queueName + '.QueueCreated',
            function () {
              log.info(queueName + ' once available');
              dependencyManager.push(queueName);
              cb();
            });
        }
      }
    }
  };
}

var worker = new WorkerFactory(app);
exports = module.exports = worker;

// start the worker if `$ node server.js`
if (require.main === module) {
  worker.init();
  process.on('SIGINT', function () {
    process.exit();
  });
  process.on('uncaughtException', function (err) {
    // Log the error
    log.fatal(err, 'Uncaught Exception in worker.js');
    process.exit();
  });
}
